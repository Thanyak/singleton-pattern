package bcas.ap.dp.creational.singleton;

public class SingletonObjDemo {

	public static void main(String[] args) {
	SingletonObj obj=SingletonObj.getInstance();
	System.out.println(obj);

	SingletonObj.setNull();
	
	SingletonObj obj2=SingletonObj.getInstance();
	System.out.println(obj2);
	}

}
